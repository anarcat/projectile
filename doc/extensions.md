## Extensions

There are a number of packages that built on top of the basic functionality provided by Projectile:

* [counsel-projectile](https://github.com/ericdanan/counsel-projectile) provides Ivy integration
* [helm-projectile](https://github.com/bbatsov/helm-projectile)
  provides Helm integration (`apt-get install elpa-helm-projectile`)
* [persp-projectile](https://github.com/bbatsov/persp-projectile)
  provides perspective.el integration (`apt-get install elpa-persp-projectile`)
* [projectile-rails](https://github.com/asok/projectile-rails) provides extra functionality for Ruby on Rails projects
